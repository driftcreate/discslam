import React, { Component } from 'react';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import LinearGradient from 'react-native-linear-gradient';
import Home from './src/screens/Home';

// we will use these two screens later in our AppNavigator
import LiveTeams from './src/screens/LiveTeams';
import GameOver from './src/screens/GameOver';
import GivePoints from './src/screens/GivePoints';

const AppNavigator = createStackNavigator(
  {
    Home: {
      screen: Home,
      navigationOptions: () => ({
        headerBackground: (
          <LinearGradient
            colors={['#F46D94', '#FF4078']}
            style={{ flex: 1 }}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
          />
        )
      }),
    },
    LiveTeams: {
      screen: LiveTeams,
      navigationOptions: () => ({
        headerBackground: (
          <LinearGradient
            colors={['#F46D94', '#FF4078']}
            style={{ flex: 1 }}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
          />
        )
      }),
    },
    GivePoints: {
      screen: GivePoints,
      navigationOptions: () => ({
        headerBackground: (
          <LinearGradient
            colors={['#F46D94', '#FF4078']}
            style={{ flex: 1 }}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
          />
        )
      }),
    },
    GameOver: {
      screen: GameOver,
      navigationOptions: () => ({
        headerBackground: (
          <LinearGradient
            colors={['#F46D94', '#FF4078']}
            style={{ flex: 1 }}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
          />
        )
      }),
    }
  },
  {
    initialRouteName: 'Home',
    // headerMode: 'none'
  }
);

const AppContainer = createAppContainer(AppNavigator);

export default class App extends Component {
  render() {
    return <AppContainer />;
  }
}