import React, { Component } from 'react';
import {
	ScrollView,
  View,
  Header,
  Text,
  TouchableHighlight,
  StyleSheet,
  TextInput,
  Alert,
  Button
} from 'react-native';

import SelectPlayers from '../components/SelectPlayers';
import LeaderBoard from '../components/LeaderBoard';

import { noTimeout } from '../noTimeout';

export default class Home extends Component {
  constructor(props){
    super(props);
  }

  render() {
    // let disabled = true;
    // if (selectedArrayOBJ.getArray().length >= 4) {
    //   disabled = false;
    // }
    return (
      <ScrollView style={styles.main}>
      	<SelectPlayers navigation={this.props.navigation} />
        <LeaderBoard />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    padding: 30,
    paddingBottom:60,
    flexDirection: 'column'
  },
  itemInput: {
    height: 50,
    padding: 4,
    marginRight: 5,
    fontSize: 23,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 8,
    color: 'white'
  },
  buttonText: {
    fontSize: 18,
    color: '#111',
    alignSelf: 'center'
  },
  button: {
    height: 45,
    flexDirection: 'row',
    backgroundColor: 'white',
    borderColor: 'white',
    borderWidth: 1,
    borderRadius: 8,
    marginBottom: 10,
    marginTop: 10,
    alignSelf: 'stretch',
    justifyContent: 'center'
  },
  selectedItemsButton: {
    backgroundColor:'#FF4078',
    alignItems: 'center',
    justifyContent: 'center',
    height:96,
    marginTop:50
  },
  selectedItemsButton_disabled: {
    backgroundColor:'#EDEDED',
    alignItems: 'center',
    justifyContent: 'center',
    height:96,
    marginTop:50
  },
  selectedItemsButton_Text: {
    color:'#ffffff',
    fontSize:22,
    fontWeight:'bold',
  }
});