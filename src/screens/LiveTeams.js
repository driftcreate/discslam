import React, { Component } from 'react';
import {
  ScrollView,
  View,
  Header,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Alert,
  Button
} from 'react-native';

import SelectPlayers from '../components/SelectPlayersTeam';
import Teams from '../components/Teams';

import {
  StackNavigator,
} from 'react-navigation';

export default class LiveTeams extends Component {
  render() {
    return (
      <ScrollView style={styles.main}>
      	<SelectPlayers navigation={this.props.navigation} />
      	<Teams />
        <TouchableOpacity 
          style={styles.startGameButton}
          onPress={() => this.props.navigation.navigate('GivePoints')}>
          <Text style={styles.startGameButton_Text}>Let's Do This</Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    padding: 30,
    paddingBottom:120,
    flexDirection: 'column'
  },
  startGameButton: {
    backgroundColor:'#FF4078',
    alignItems: 'center',
    justifyContent: 'center',
    height:96,
    marginTop:50,
    marginBottom:50
  },
  startGameButton_Text: {
    color:"#ffffff",
    fontWeight:"bold",
    fontSize:22
  }
});