import React, { Component } from 'react';
import { View, Text, ScrollView, StyleSheet } from 'react-native';

import {
  StackNavigator,
} from 'react-navigation';

import Winners from '../components/Winners';

export default class GivePoints extends Component {
  render() {
    return (
      <ScrollView style={styles.main}>
      	<Winners navigation={this.props.navigation}/>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    padding: 30,
    paddingBottom:120,
    flexDirection: 'column'
  },
  startGameButton: {
    backgroundColor:'#FF4078',
    alignItems: 'center',
    justifyContent: 'center',
    height:96,
    marginTop:50,
    marginBottom:50
  },
  startGameButton_Text: {
    color:"#ffffff",
    fontWeight:"bold",
    fontSize:22
  }
});