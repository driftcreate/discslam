import React, {Component} from 'react';
import {StyleSheet,Text,View,TouchableHighlight,TextInput,Alert, Image} from 'react-native';
import { db } from '../config';

export default class AddPlayer extends Component {
  state = {
    name: '',
    points: '',
    valid: '',
  };
  handleChange = e => {
    this.setState({
      name: e.nativeEvent.text,
      points: 0,
      valid: true
    });
  };
  handleSubmit = () => {
    var playerName = this.state.name
    if (playerName.length == 0) {
      Alert.alert('Please enter a name before submitting');
    } else {
      db.ref('/players').push({
        name: this.state.name,
        points: 0
      });
      //this.textInput.clear();
    }
  };
  render(){
    return ( 
      <View style={styles.inputContainer}>
      	<TextInput 
          placeholder="Add New Player..." 
          placeholderTextColor="#ffffff" 
          style={styles.itemInput} 
          onChange={this.handleChange} 
          ref={input => { this.textInput = input }}
        />
        <TouchableHighlight
          style={styles.button}
          underlayColor="white"
          onPress={this.handleSubmit}
          //disabled={true}
        >
          <Image source={require('../images/add.png')} style={styles.checkBoxImage} />
        </TouchableHighlight>
      </View>
    );
  }
}

var styles = StyleSheet.create({
  inputContainer: {
    backgroundColor: '#FF4078',
    paddingTop:10,
    paddingBottom:10,
    paddingLeft:15,
    paddingRight:20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  itemInput: {
    color:'#ffffff',
    fontWeight: 'bold',
    fontSize:18,
    minWidth:250,
  },
  checkBoxImage: {
    width:25
  }
});