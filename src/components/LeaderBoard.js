import React, {Component} from 'react';
import {StyleSheet,Text,View,Image} from 'react-native';
import PropTypes from 'prop-types';
import { db } from '../config';

let itemsRef = db.ref('/players').orderByChild('points').limitToLast(5);

export default class LeaderBoard extends Component {
	static propTypes = {
		players: PropTypes.array.isRequired
	};
	constructor(props) {
	  super(props);
	  this.state = { players:[] }
	}
	componentDidMount() {
    itemsRef.on('value', snapshot => {
    	let playas=[];
    	snapshot.forEach(player => {
	      let data = player.val();
	      playas.push(player.val());
	    });
	    this.setState({ players: playas });
    });
  }
	render(){
		var i = 0;
		return ( 
			<View>
				<Text style={styles.title}>Leader Board</Text>

				{this.state.players.length > 0 ? ( 
          <View style={styles.items}>
            {this.state.players.reverse().map((item, index) => {
            	i++;
            	if (i == 1) {
            		return (
            			<View style={styles.leaderBoardPlayer}>
	              		<View style={styles.namePlace}>
	              			<Image style={styles.crown} source={require('../images/crown.png')}></Image>
	                		<Text style={styles.leaderBoardText_name}>{item.name}</Text>
	                	</View>
	                	<View style={styles.pointsPlace}>
	                		<Text style={styles.leaderBoardText_points}>{item.points}</Text>
	                		<Text style={styles.leaderBoardText_pts}>PTS</Text>
	                	</View>
	                </View>
            		);
            	} else {
	              return (
	              	<View style={styles.leaderBoardPlayer}>
	              		<View style={styles.namePlace}>
	              			<Text style={styles.leaderBoardText_place}>0{i}</Text>
	                		<Text style={styles.leaderBoardText_name}>{item.name}</Text>
	                	</View>
	                	<View style={styles.pointsPlace}>
	                		<Text style={styles.leaderBoardText_points}>{item.points}</Text>
	                		<Text style={styles.leaderBoardText_pts}>PTS</Text>
	                	</View>
	                </View>
	              );
	            }
            })}
          </View>
        ) : (
          <Text style={styles.text}>No players</Text>
        )}

			</View>
		);
	}
}

var styles = StyleSheet.create({
	title: {
		fontSize: 25,
		color:'#FF4078',
		fontWeight: 'bold',
		marginTop:50
	},
	leaderBoardPlayer: {
		padding:20,
		paddingLeft:0,
		paddingRight:0,
		borderBottomColor:'#C3C3C3',
		borderBottomWidth:1,
		display:'flex',
		flexDirection:'row',
		justifyContent:'space-between'
	},
	namePlace: {
		display:'flex',
		flexDirection:'row',
		justifyContent:'space-between'
	},
	pointsPlace: {
		display:'flex',
		flexDirection:'row',
		justifyContent:'space-between'
	},
	leaderBoardText_place: {
		color:'#909090',
		fontSize:14,
		width:50
	},
	leaderBoardText_name: {
		color:'#909090',
		fontSize:14,
	},
	leaderBoardText_points: {
		color:'#909090',
		fontSize:14,
	},
	leaderBoardText_pts: {
		color:'#909090',
		fontSize:14,
		marginLeft:15
	},
	crown: {
		width:30,
		height:18,
		marginRight:20,
	}
});