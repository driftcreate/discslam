import React, {Component} from 'react';
import {Button,StyleSheet,Text,View,LayoutAnimation,Platform,UIManager,TouchableOpacity,TouchableHighlight,Image,Alert} from 'react-native';
import PropTypes from 'prop-types';
import AddPlayer from './AddPlayer';
import { db } from '../config';

import {
  StackNavigator,
} from 'react-navigation';

let itemsRef = db.ref('/players');

class Selected_Items_Array {
  constructor() {
    selectedItemsArray = [];
  }

  pushItem(option) {
    selectedItemsArray.push(option);
  }

  getArray() {
    return selectedItemsArray;
  }
}

class Checkbox extends Component {
  constructor(props) {
    super(props);
    this.state = { checked: null, disabled: true };
  }

  componentDidMount() {
    if (this.props.checked) {
      this.setState({ checked: true, disabled: false }, () => {
        this.props.selectedArrayObject.pushItem({
          'key': this.props.keyValue,
          'label': this.props.label,
          'value': this.props.value,
          'points': this.props.points
        });
      });
    }
    else {
      this.setState({ checked: false, disabled: true });
    }
  }

  toggleState(key, label, value, points) {
    this.setState({ checked: !this.state.checked }, () => {
      if (this.state.checked) {
        this.props.selectedArrayObject.pushItem({ 'key': key, 'label': label, 'value': value, 'points': points });
      } else {
        this.props.selectedArrayObject.getArray().splice(this.props.selectedArrayObject.getArray().findIndex(x => x.key == key), 1);
      }
    });
    this.props.onToggle(this.state.disabled);
  }

  render() {
    return (
      <TouchableHighlight
        onPress={this.toggleState.bind(this, this.props.keyValue, this.props.label, this.props.value, this.props.points)}
        underlayColor="transparent">

        <View style={styles.player}>

          <Text style={styles.playerName}>{this.props.label}</Text>
          <Text style={styles.playerPoints}>{this.props.points}</Text>

          <View style={styles.playerAdd}>
            {
              (this.state.checked)
                ?
                (<View style={styles.checkedView}>
                  <Image source={require('../images/remove.png')} style={styles.checkBoxImage} />
                </View>)
                :
                (<View style={styles.uncheckedView}>
                  <Image source={require('../images/add.png')} style={styles.checkBoxImage} />
                </View>)
            }
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

export default class SelectPlayers extends Component {
  static propTypes = {
    players: PropTypes.array.isRequired
  };

	constructor(props) {
	  super(props);
	 
	  this.state = { expanded: false, players:[], disabled: true }

	  this.icons = {     
      'up'    : require('../images/arrow-up.png'),
      'down'  : require('../images/arrow-down.png')
    };
	 
	  if (Platform.OS === 'android') {
	    UIManager.setLayoutAnimationEnabledExperimental(true);
	  }

    selectedArrayOBJ = new Selected_Items_Array();
	}

  onChildToggle = (disabled) => {
    var disabled = this.state.disabled;
    if (selectedArrayOBJ.getArray().length > 2) {
      this.setState({ disabled: false });
    } if (selectedArrayOBJ.getArray().length < 3) {
      this.setState({ disabled: true });
    }
  }

  componentDidMount() {
    itemsRef.on('value', snapshot => {
      let data = snapshot.val();
      let players = Object.values(data);
      this.setState({ players });
    });
  }

	changeLayout = () => {
	  LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
	  this.setState({ expanded: !this.state.expanded });
	}

  getSelectedItems = () => {
    if (selectedArrayOBJ.getArray().length == 0) {
      Alert.alert('No players selected.');
    } else if (selectedArrayOBJ.getArray().length == 1) {
      Alert.alert('Not enough players.');
    } else if (selectedArrayOBJ.getArray().length == 2) {
      Alert.alert('Not enough players.');
    } else if (selectedArrayOBJ.getArray().length == 3) {
      Alert.alert('Not enough players.');
    } else if (selectedArrayOBJ.getArray().length == 5) {
      Alert.alert('Please select an even amount of players.');
    } else if (selectedArrayOBJ.getArray().length == 7) {
      Alert.alert('Please select an even amount of players.');
    } else if (selectedArrayOBJ.getArray().length == 9) {
      Alert.alert('Please select an even amount of players.');
    } else {
      let gameRef = db.ref('/live-game');
      gameRef.remove();

      this.setState(() => {
        return {
          selectedItems: selectedArrayOBJ.getArray().map(item => item.value).join()
        }
      });

      db.ref('/games').push({
        players: selectedArrayOBJ.getArray()
      });

      let arr = selectedArrayOBJ.getArray();

      function shuffle(arr) {
        var i,
            j,
            temp;
        for (i = arr.length - 1; i > 0; i--) {
            j = Math.floor(Math.random() * (i + 1));
            temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
        return arr;    
      };

      let shuffled = shuffle(arr);

      var i,j,temparray,chunk = 2;
      var c = 1;
      for (i=0,j=shuffled.map(item => item.value).join().length; i<j; i+=chunk) {
        temparray = shuffled.slice(i,i+chunk);
        var teamCount = "team"+c
        db.ref('/live-game').push({
          [teamCount]: temparray
        });
        c++;
      }
    }
    this.props.navigation.push('LiveTeams');
  }

  render(){
  	let icon = this.icons['down'];
    if(this.state.expanded){
      icon = this.icons['up'];   
    }
    return ( 
    	<View style={styles.selectPlayers}>
        <Text style={styles.title}>Select Players</Text>
				<View style={styles.container}>
		      <View style={styles.btnTextHolder}>
		        <TouchableOpacity activeOpacity={0.8} onPress={this.changeLayout} style={styles.Btn}>
		          <Text style={styles.btnText}>See All Players</Text>
		          <Image style={styles.btnImage} source={icon}></Image>
		        </TouchableOpacity>
		        <View style={{ height: this.state.expanded ? null : 0, overflow: 'hidden' }}>
              <View style={styles.dropdownChild}>
                {this.state.players.length > 0 ? (

                  
                  <View style={styles.items}>
                    {this.state.players.map((item, index) => {
                      return (

                        <Checkbox size={30}
                          keyValue={1}
                          selectedArrayObject={selectedArrayOBJ}
                          checked={false}
                          color="#0091EA"
                          labelColor="#0091EA"
                          label={item.name}
                          value={item.name}
                          points={item.points}
                          onToggle={this.onChildToggle} />
                      );
                    })}

                    <AddPlayer />
                  </View>


                ) : (
                  <Text style={styles.text}>No players</Text>
                )}
              </View>
		        </View>
		      </View>
		    </View>
        <TouchableOpacity 
          style={this.state.disabled ? styles.selectedItemsButton_disabled : styles.selectedItemsButton } 
          onPress={this.getSelectedItems} 
          disabled={this.state.disabled}>
          <Text style={styles.selectedItemsButton_Text}>Randomize Players</Text>
        </TouchableOpacity>
		  </View>
    );
  }
}

var styles = StyleSheet.create({
	title: {
    marginBottom: 15,
    fontSize: 25,
    color:'#FF4078',
    fontWeight: 'bold'
  },
  Btn: {
  	backgroundColor: '#F7F7F7',
  	borderColor: '#C3C3C3',
  	borderWidth: 1,
  	padding:20,
  	display: 'flex',
  	flexDirection: 'row',
  	justifyContent: 'space-between',
  	alignItems: 'center'
  },
  btnText: {
  	color: '#909090',
  	fontSize: 18
  },
  btnImage: {
  	width:17,
  	height:13
  },
  selectedItemsButton: {
    backgroundColor:'#FF4078',
    alignItems: 'center',
    justifyContent: 'center',
    height:96,
    marginTop:50
  },
  selectedItemsButton_disabled: {
    backgroundColor:'#EDEDED',
    alignItems: 'center',
    justifyContent: 'center',
    height:96,
    marginTop:50
  },
  selectedItemsButton_Text: {
    color:"#ffffff",
    fontWeight:"bold",
    fontSize:22
  },
  player: {
    backgroundColor: '#F7F7F7',
    borderColor: '#C3C3C3',
    borderWidth: 1,
    borderTopWidth:0,
    padding:20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  playerName: {
    color: '#909090',
    fontSize: 18
  },
  playerPoints: {
    color: '#909090',
    fontSize: 18,
    marginLeft:'auto'
  },
  playerAdd: {
    width:25,
    marginLeft: 40
  },
  btnImage: {
    width:17,
    height:13
  },
  checkBoxImage: {
    width:25,
    height:25
  },
  selectedItemsButton: {
    backgroundColor:'#FF4078',
    alignItems: 'center',
    justifyContent: 'center',
    height:96,
    marginTop:50
  },
  selectedItemsButton_disabled: {
    backgroundColor:'#EDEDED',
    alignItems: 'center',
    justifyContent: 'center',
    height:96,
    marginTop:50
  },
});