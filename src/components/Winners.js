import React, {Component} from 'react';
import {StyleSheet,Text,View,Image,ScrollView, TouchableOpacity, TextInput} from 'react-native';
import PropTypes from 'prop-types';
import { db } from '../config';

import RNRestart from 'react-native-restart';

import {
  StackNavigator,
} from 'react-navigation';

let itemsRef = db.ref('/live-game').orderByKey();

export default class Teams extends Component {
	static propTypes = {
		team1: PropTypes.array.isRequired,
		team2: PropTypes.array.isRequired,
		team3: PropTypes.array.isRequired,
		team4: PropTypes.array.isRequired
	};
	constructor(props) {
	  super(props);
	  this.state = { team1:[], team2:[], team3:[], team4:[], teamAmount:null, teamAmountOverride:null }
	}
	componentDidMount() {
		itemsRef.on('value', snapshot => {
			let team1=[];
			let team2=[];
			let team3=[];
			let team4=[];
			let teamAmount=[];
			snapshot.forEach(function(game) {
				game.child("team1").forEach(function(t1game){
					team1.push(t1game.val());
				});
				game.child("team2").forEach(function(t2game){
					team2.push(t2game.val());
				});
				game.child("team3").forEach(function(t3game){
					team3.push(t3game.val());
				});
				game.child("team4").forEach(function(t4game){
					team4.push(t4game.val());
				});
				teamAmount.push(game.numChildren());
			});

			if (teamAmount.length >= 3 && this.state.teamAmountOverride != 1) {
				let teamAmount = 2;
				this.setState({ team1: team1, team2: team2, team3: team3, team4: team4, teamAmount: teamAmount });
				console.log('points: '+teamAmount);
				console.log('override1: '+this.state.teamAmountOverride);
			} else {
				let teamAmount = 1;
				this.setState({ team1: team1, team2: team2, team3: team3, team4: team4, teamAmount: teamAmount });
				console.log('points: '+teamAmount);
				console.log('override2: '+this.state.teamAmountOverride);
			}

			// this.setState({ team1: team1, team2: team2, team3: team3, team4: team4, teamAmount: teamAmount });
			// console.log(teamAmount);
		});
  	}
  	handleSubmitOne = () => {
	    if (this.state.teamAmount == 2 && this.state.teamAmountOverride != 1) {

		    {this.state.team1.map((item, index) => {

		    	// add 2 points to current amount of points
		    	var query = db.ref('/players').orderByChild('name').equalTo(item.value);
		    	query.once('child_added', function(snapshot) {
		    		snapshot.ref.update({points: item.points + 2});
		    	});
			    
	        })}

	        this.setState({
			    teamAmountOverride: 1
			}, () => console.log('override3: '+this.state.teamAmountOverride));
		} else {
			{this.state.team1.map((item, index) => {

		    	// add 2 points to current amount of points
		    	var query = db.ref('/players').orderByChild('name').equalTo(item.value);
		    	query.once('child_added', function(snapshot) {
		    		snapshot.ref.update({points: item.points + 1});
		    	});
			    
	        })}
	        RNRestart.Restart();
		}

        itemsRef.on('value', snapshot => {
			snapshot.forEach(function(game) {
				if (game.hasChild("team1") == true) {
					let key = game.child("team1").ref.parent.key;
					db.ref('/live-game/'+key).remove();
				} else {
				}
			});
		});
	};
	handleSubmitTwo = () => {
	    if (this.state.teamAmount == 2 && this.state.teamAmountOverride != 1) {

		    {this.state.team2.map((item, index) => {

		    	// add 2 points to current amount of points
		    	var query = db.ref('/players').orderByChild('name').equalTo(item.value);
		    	query.once('child_added', function(snapshot) {
		    		snapshot.ref.update({points: item.points + 2});
		    	});
			    
	        })}

	        this.setState({
			    teamAmountOverride: 1
			}, () => console.log('override3: '+this.state.teamAmountOverride));
		} else {
			{this.state.team2.map((item, index) => {

		    	// add 2 points to current amount of points
		    	var query = db.ref('/players').orderByChild('name').equalTo(item.value);
		    	query.once('child_added', function(snapshot) {
		    		snapshot.ref.update({points: item.points + 1});
		    	});
			    
	        })}
	        RNRestart.Restart();
		}

        itemsRef.on('value', snapshot => {
			snapshot.forEach(function(game) {
				if (game.hasChild("team2") == true) {
					let key = game.child("team2").ref.parent.key;
					db.ref('/live-game/'+key).remove();
				} else {
				}
			});
		});
	};
	handleSubmitThree = () => {
	    if (this.state.teamAmount == 2 && this.state.teamAmountOverride != 1) {

		    {this.state.team3.map((item, index) => {

		    	// add 2 points to current amount of points
		    	var query = db.ref('/players').orderByChild('name').equalTo(item.value);
		    	query.once('child_added', function(snapshot) {
		    		snapshot.ref.update({points: item.points + 2});
		    	});
			    
	        })}

	        this.setState({
			    teamAmountOverride: 1
			}, () => console.log('override3: '+this.state.teamAmountOverride));
		} else {
			{this.state.team3.map((item, index) => {

		    	// add 2 points to current amount of points
		    	var query = db.ref('/players').orderByChild('name').equalTo(item.value);
		    	query.once('child_added', function(snapshot) {
		    		snapshot.ref.update({points: item.points + 1});
		    	});
			    
	        })}
	        RNRestart.Restart();
		}

        itemsRef.on('value', snapshot => {
			snapshot.forEach(function(game) {
				if (game.hasChild("team3") == true) {
					let key = game.child("team3").ref.parent.key;
					db.ref('/live-game/'+key).remove();
				} else {
				}
			});
		});
	};
	handleSubmitFour = () => {
	    if (this.state.teamAmount == 2 && this.state.teamAmountOverride != 1) {

		    {this.state.team4.map((item, index) => {

		    	// add 2 points to current amount of points
		    	var query = db.ref('/players').orderByChild('name').equalTo(item.value);
		    	query.once('child_added', function(snapshot) {
		    		snapshot.ref.update({points: item.points + 2});
		    	});
			    
	        })}

	        this.setState({
			    teamAmountOverride: 1
			}, () => console.log('override3: '+this.state.teamAmountOverride));
		} else {
			{this.state.team4.map((item, index) => {

		    	// add 2 points to current amount of points
		    	var query = db.ref('/players').orderByChild('name').equalTo(item.value);
		    	query.once('child_added', function(snapshot) {
		    		snapshot.ref.update({points: item.points + 1});
		    	});
			    
	        })}
	        RNRestart.Restart();
		}

        itemsRef.on('value', snapshot => {
			snapshot.forEach(function(game) {
				if (game.hasChild("team4") == true) {
					let key = game.child("team4").ref.parent.key;
					db.ref('/live-game/'+key).remove();
				} else {
				}
			});
		});
	};
	render(){
		var i = 0;
		return ( 
			<View>
				{this.state.teamAmountOverride != 1 ?
					<Text style={styles.title}>Which Team Won?</Text> : <Text style={styles.title}>Which Team Got 2nd?</Text>
				}
				{this.state.team1.length > 0 ? 
					<TouchableOpacity 
						style={styles.pointButton}
						onPress={this.handleSubmitOne}>
						<Text style={styles.buttonTitle}>Team 1</Text>
						<View style={styles.team}>
				            {this.state.team1.map((item, index) => {
				            	i++;
			            		return (
			            			<Text style={styles.pointButton_Text}>{item.value} {i == 1 ? 'x ' : null}</Text>
			            		);
				            })}
				        </View>
				    </TouchableOpacity>
				    : null
				}
				{this.state.team2.length > 0 ? 
				    <TouchableOpacity 
						style={styles.pointButton}
						onPress={this.handleSubmitTwo}>
						<Text style={styles.buttonTitle}>Team 2</Text>
						<View style={styles.team}>
				            {this.state.team2.map((item, index) => {
				            	i++;
			            		return (
			            			<Text style={styles.pointButton_Text}>{item.value} {i == 3 ? 'x ' : null}</Text>
			            		);
				            })}
				        </View>
				    </TouchableOpacity>
			    	: null
				}
			    {this.state.team3.length > 0 ? 
				    <TouchableOpacity 
						style={styles.pointButton}
						onPress={this.handleSubmitThree}>
						<Text style={styles.buttonTitle}>Team 3</Text>
						<View style={styles.team}>
				            {this.state.team3.map((item, index) => {
				            	i++;
			            		return (
			            			<Text style={styles.pointButton_Text}>{item.value} {i == 5 ? 'x ' : null}</Text>
			            		);
				            })}
				        </View>
				    </TouchableOpacity>
				   	: null
				}
				{this.state.team4.length > 0 ? 
				    <TouchableOpacity 
						style={styles.pointButton}
						onPress={this.handleSubmitFour}>
						<Text style={styles.buttonTitle}>Team 4</Text>
						<View style={styles.team}>
				            {this.state.team4.map((item, index) => {
				            	i++;
			            		return (
			            			<Text style={styles.pointButton_Text}>{item.value} {i == 7 ? 'x ' : null}</Text>
			            		);
				            })}
				        </View>
				    </TouchableOpacity>
				    : null
				}
			</View>
		);
	}
}

var styles = StyleSheet.create({
	title: {
		fontSize: 25,
		color:'#FF4078',
		fontWeight: 'bold',
	},
	leaderBoardPlayer: {
		padding:20,
		paddingLeft:0,
		paddingRight:0,
		borderBottomColor:'#C3C3C3',
		borderBottomWidth:1,
		display:'flex',
		flexDirection:'row',
		justifyContent:'space-between'
	},
	namePlace: {
		display:'flex',
		flexDirection:'row',
		justifyContent:'space-between'
	},
	pointsPlace: {
		display:'flex',
		flexDirection:'row',
		justifyContent:'space-between'
	},
	leaderBoardText_place: {
		color:'#909090',
		fontSize:14,
		width:50
	},
	leaderBoardText_name: {
		color:'#909090',
		fontSize:14,
	},
	leaderBoardText_points: {
		color:'#909090',
		fontSize:14,
	},
	leaderBoardText_pts: {
		color:'#909090',
		fontSize:14,
		marginLeft:15
	},
	crown: {
		width:30,
		height:18,
		marginRight:20,
	},
	pointButton: {
	    backgroundColor:'#FF4078',
	    alignItems: 'center',
	    justifyContent: 'center',
	    height:96,
	    marginTop:25,
	},
	buttonTitle: {
	    color:"#ffffff",
	    fontWeight:"bold",
	    fontSize:22
	},
	team: {
		display:'flex',
		flexDirection:'row'
	},
	pointButton_Text: {
	    color:"#ffffff",
	    fontWeight:"bold",
	    fontSize:18
	}
});