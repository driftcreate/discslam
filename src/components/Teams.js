import React, {Component} from 'react';
import {StyleSheet,Text,View,Image,ScrollView} from 'react-native';
import PropTypes from 'prop-types';
import { db } from '../config';

let itemsRef = db.ref('/live-game').orderByKey();

export default class Teams extends Component {
	static propTypes = {
		team1: PropTypes.array.isRequired,
		team2: PropTypes.array.isRequired,
		team3: PropTypes.array.isRequired,
		team4: PropTypes.array.isRequired
	};
	constructor(props) {
	  super(props);
	  this.state = { team1:[], team2:[], team3:[], team4:[] }
	}
	componentDidMount() {
		itemsRef.on('value', snapshot => {
			let team1=[];
			let team2=[];
			let team3=[];
			let team4=[];
			snapshot.forEach(function(game) {
				game.child("team1").forEach(function(t1game){
					team1.push(t1game.val());
				});
				game.child("team2").forEach(function(t2game){
					team2.push(t2game.val());
				});
				game.child("team3").forEach(function(t3game){
					team3.push(t3game.val());
				});
				game.child("team4").forEach(function(t4game){
					team4.push(t4game.val());
				});
			});
			this.setState({ team1: team1, team2: team2, team3: team3, team4: team4 });
			//console.log(team1);
		});
  	}
	render(){
		var i = 0;
		return ( 
			<View>
				<View>
					<Text style={styles.title}>Team 1</Text>
			        <View style={styles.items}>
			            {this.state.team1.map((item, index) => {
			            	i++;
		            		return (
		            			<View style={styles.leaderBoardPlayer}>
			              		<View style={styles.namePlace}>
			              			<Text style={styles.leaderBoardText_place}>0{i}</Text>
			                		<Text style={styles.leaderBoardText_name}>{item.value}</Text>
			                	</View>
			                	<View style={styles.pointsPlace}>
			                		<Text style={styles.leaderBoardText_points}>{item.points}</Text>
			                		<Text style={styles.leaderBoardText_pts}>PTS</Text>
			                	</View>
			                </View>
		            		);
			            	
			            })}
			        </View>
			    </View>
			    <View>
					<Text style={styles.title}>Team 2</Text>
			        <View style={styles.items}>
			            {this.state.team2.map((item, index) => {
			            	i++;
		            		return (
		            			<View style={styles.leaderBoardPlayer}>
			              		<View style={styles.namePlace}>
			              			<Text style={styles.leaderBoardText_place}>0{i}</Text>
			                		<Text style={styles.leaderBoardText_name}>{item.value}</Text>
			                	</View>
			                	<View style={styles.pointsPlace}>
			                		<Text style={styles.leaderBoardText_points}>{item.points}</Text>
			                		<Text style={styles.leaderBoardText_pts}>PTS</Text>
			                	</View>
			                </View>
		            		);
			            })}
			        </View>
			    </View>
			    {this.state.team3.length > 0 ? 
				    <View>
						<Text style={styles.title}>Team 3</Text>
				        <View style={styles.items}>
				            {this.state.team3.map((item, index) => {
				            	i++;
			            		return (
			            			<View style={styles.leaderBoardPlayer}>
				              		<View style={styles.namePlace}>
				              			<Text style={styles.leaderBoardText_place}>0{i}</Text>
				                		<Text style={styles.leaderBoardText_name}>{item.value}</Text>
				                	</View>
				                	<View style={styles.pointsPlace}>
				                		<Text style={styles.leaderBoardText_points}>{item.points}</Text>
				                		<Text style={styles.leaderBoardText_pts}>PTS</Text>
				                	</View>
				                </View>
			            		);
				            })}
				        </View>
				    </View>
				   	: null
				}
				{this.state.team4.length > 0 ? 
				    <View>
						<Text style={styles.title}>Team 4</Text>
				        <View style={styles.items}>
				            {this.state.team4.map((item, index) => {
				            	i++;
			            		return (
			            			<View style={styles.leaderBoardPlayer}>
				              		<View style={styles.namePlace}>
				              			<Text style={styles.leaderBoardText_place}>0{i}</Text>
				                		<Text style={styles.leaderBoardText_name}>{item.value}</Text>
				                	</View>
				                	<View style={styles.pointsPlace}>
				                		<Text style={styles.leaderBoardText_points}>{item.points}</Text>
				                		<Text style={styles.leaderBoardText_pts}>PTS</Text>
				                	</View>
				                </View>
			            		);
				            })}
				        </View>
				    </View>
				    : null
				}
			</View>
		);
	}
}

var styles = StyleSheet.create({
	title: {
		fontSize: 25,
		color:'#FF4078',
		fontWeight: 'bold',
		marginTop:50
	},
	leaderBoardPlayer: {
		padding:20,
		paddingLeft:0,
		paddingRight:0,
		borderBottomColor:'#C3C3C3',
		borderBottomWidth:1,
		display:'flex',
		flexDirection:'row',
		justifyContent:'space-between'
	},
	namePlace: {
		display:'flex',
		flexDirection:'row',
		justifyContent:'space-between'
	},
	pointsPlace: {
		display:'flex',
		flexDirection:'row',
		justifyContent:'space-between'
	},
	leaderBoardText_place: {
		color:'#909090',
		fontSize:14,
		width:50
	},
	leaderBoardText_name: {
		color:'#909090',
		fontSize:14,
	},
	leaderBoardText_points: {
		color:'#909090',
		fontSize:14,
	},
	leaderBoardText_pts: {
		color:'#909090',
		fontSize:14,
		marginLeft:15
	},
	crown: {
		width:30,
		height:18,
		marginRight:20,
	}
});